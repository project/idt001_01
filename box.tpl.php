<div class="box">  
<div class="idt-Block clear-block block block-<?php print $block->module ?>" id="block-<?php print $block->module .'-'. $block->delta; ?>">
      <div class="idt-Block-body">
      
    <?php if ($title): ?>
<div class="idt-BlockHeader">
          <div class="l"></div>
          <div class="r"></div>
          <div class="idt-header-tag-icon">
              <div class="t">
        <?php echo $title; ?>
</div>
          </div>
      </div>
    <?php endif; ?>
<div class="idt-BlockContent content">
        <div class="idt-BlockContent-tl"></div>
        <div class="idt-BlockContent-tr"></div>
        <div class="idt-BlockContent-bl"></div>
        <div class="idt-BlockContent-br"></div>
        <div class="idt-BlockContent-tc"></div>
        <div class="idt-BlockContent-bc"></div>
        <div class="idt-BlockContent-cl"></div>
        <div class="idt-BlockContent-cr"></div>
        <div class="idt-BlockContent-cc"></div>
        <div class="idt-BlockContent-body">
    
      <?php echo $content; ?>

        </div>
    </div>
        

      </div>
  </div>
      
</div>
